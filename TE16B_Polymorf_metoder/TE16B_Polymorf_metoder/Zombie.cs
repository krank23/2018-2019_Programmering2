﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16B_Polymorf_metoder
{
    class Zombie : Human
    {
        public Zombie()
        {
            maxHP = 5;
            hp = maxHp;
        }

        public override void GetHealthPowerup()
        {
            hp = Math.Max(hp - 5, 0);
        }


    }
}
