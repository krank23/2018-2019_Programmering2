﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {

        float vertical = Input.GetAxisRaw("Vertical");

        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        rb.velocity = Vector2.up * vertical * 4;


	}
}
