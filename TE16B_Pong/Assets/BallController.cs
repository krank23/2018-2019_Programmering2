﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        rb.AddForce((Vector2.right + Vector2.down) * 750);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
