﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16B_Konstruktor
{
    class Sin
    {
        public string name;
        public int severity;

        public Sin(string n)
        {
            Console.WriteLine("Sin created!");

            Random gen = new Random();
            severity = gen.Next(1, 11);
            name = n;
        }

    }
}
