﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16B_Arv
{
    class Player : Character
    {
        private int hp;

        public Player(string n, string i)
        {
            name = n;
            interest = i;

            Interact("evighetsmaskin");
        }

        public string GetName()
        {
            return name;
        }

    }
}
