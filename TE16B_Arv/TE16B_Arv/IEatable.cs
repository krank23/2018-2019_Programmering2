﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16B_Arv
{
    interface IEatable
    {
        void Eat();
    }
}
