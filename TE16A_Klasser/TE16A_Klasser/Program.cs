﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16A_Klasser
{
    class Program
    {
        static void Main(string[] args)
        {
            Ball b1 = new BigBall();


            IceCoffee i1 = new IceCoffee();
            IceCoffee i2 = new IceCoffee();

            i2.damage = 12;

            Console.WriteLine(i1.damage);
            Console.WriteLine(i2.damage);

            i1.SetName("Herbert");

            i1.Throw();


            Console.ReadLine();
        }
    }
}
