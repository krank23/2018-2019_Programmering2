﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16A_Klasser
{
    class IceCoffee
    {
        public bool isDrinkable = false;
        public bool isThrowable = true;

        public int damage = 6;

        private string secretName = "Kjell-Arne";

        public void Throw()
        {
            Console.WriteLine("weeeeee!");
            Console.WriteLine(secretName);
        }

        public void SetName(string n)
        {
            secretName = n;
        }

    }
}
