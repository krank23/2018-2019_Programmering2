﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour {

    [SerializeField]
    string axis;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        float moveY = Input.GetAxisRaw(axis);

        Vector2 newVelocity = new Vector2(0, moveY) * 10;

        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        rb.velocity = newVelocity;

	}
}
