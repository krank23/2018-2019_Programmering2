﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Rigidbody2D rigidBody = GetComponent<Rigidbody2D>();

        Vector2 force = (Vector2.right + Vector2.down) * 1000;

        rigidBody.AddForce(force);


	}

    private void Update()
    {
        Rigidbody2D rigidBody = GetComponent<Rigidbody2D>();

        rigidBody.velocity = rigidBody.velocity.normalized * 10;
    }
}
