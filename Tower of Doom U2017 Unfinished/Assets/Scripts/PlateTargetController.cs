﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlateTargetController: MonoBehaviour
{

    public abstract void Affect();
	// Den här behöver du inte göra något med.
}
