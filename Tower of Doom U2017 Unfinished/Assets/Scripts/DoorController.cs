﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : PlateTargetController
{
    private bool isEnabled = true;

    private float closeTimer = 0;
    private float closeTimerMax = 5;

    private void Update()
    {
        // Använd timern för att göra så att dörren stängs igen efter 5 sekunder
    }

    public override void Affect()
    {
        // När Affect-metoden anropas, stäng av Renderer- och Collider2D-komponenterna så objektet inte syns eller kolliderar.
        // Sätt också igång timern.
    }

}
