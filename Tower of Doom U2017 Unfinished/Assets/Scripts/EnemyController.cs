﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public GameObject bulletPrefab;

    Vector3[] directions = new Vector3[] { Vector2.up, Vector2.down, Vector2.left, Vector2.right };
    bool verticalShot = false;

    float countdown = 0;
    float countdownMax = 2;

	// Use this for initialization
	void Start () {
        // Återställ timern (countdown)
	}
	
	// Update is called once per frame
	void Update () {

        // Använd timern för att skjuta varannan sekund.
        // Skjut alltid två skott. Varannan gång uppåt+neråt, varannan gång vänster+höger.
        // Du kan använda verticalShot för att minnas vilken riktning du skjöt senast.
        // Använd metoden Shoot om du vill strukturera upp det lite =)

	}

    void Shoot(Vector3 direction)
    {
        // Instansiera ett nytt skott och rotera det i riktningen som anges av direction.
    }
}
