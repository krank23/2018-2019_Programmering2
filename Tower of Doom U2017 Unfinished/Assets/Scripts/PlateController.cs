﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateController : MonoBehaviour
{

    public PlateTargetController[] targets;

    private float coolDownTimer;
    private float coolDownTimeMax = 1f;

    private void Update()
    {
        // Räkna ner cooldownen
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Om cooldownen är klar och plattan kolliderar med spelaren, gå igenom alla objekt i targets.
        // Anropa metoden Affect på var och en av dem.
        // Återställ sedan cooldownen.
    }

}
