﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16A_Konstruktorer_Arv
{
    class Vehicle
    {
        public int speed;
        public int x;
        public int y;
        public int weight;
        public bool flying;
        
        public void Drive()
        {
            y += speed;
        }
    }
}
