﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16A_Konstruktorer_Arv
{
    class Monkey
    {
        public int hp = 10;
        public string name;

        public Monkey(string n)
        {
            name = n;

            Console.WriteLine("Ook! Ook?");
            Random gen = new Random();

            hp = gen.Next(3, 10);
        }
    }
}
