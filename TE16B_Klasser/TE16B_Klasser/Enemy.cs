﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16B_Klasser
{
    class Enemy
    {
        private int xp = 0;

        public int Level
        {
            get
            {
                return xp / 10;
            }
            private set { }
        }

        public int Xp { get; set; }



        private int mp = 0;
        public int Mp
        {
            get {
                return mp;
            }

            set
            {
                mp = value;
            }
        }

        public int hp = 100;
        public int x = 10;
        public int y = 10;

        public int xpDrop = 50;
        public int level = 1;

        private string secretName = "Kjell-Albert";

        public void Hurt(int amount)
        {
            hp -= amount;
            Console.WriteLine("Ouch! säger " + secretName);
            if (hp < 0)
            {
                hp = 0;
            }
        }

    }
}
