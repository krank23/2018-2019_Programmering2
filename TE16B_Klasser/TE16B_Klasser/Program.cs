﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16B_Klasser
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] enemyHp = new int[3] { 100, 100, 100 };
            int[] enemyX = new int[3] { 20, 10, 15};
            int[] enemyY = new int[3] { 15, 10, 20 };

            Enemy[] enemies = new Enemy[] {
                new Enemy(),
                new Enemy(),
                new Enemy()
            };

            Enemy e1 = new Enemy();
            Enemy e2 = new Enemy();

            e1.Hurt(20);
            e2.Hurt(45);

            e1.Mp = 4;

            Console.WriteLine(e1.hp);
            Console.WriteLine(e2.hp);


            Console.ReadLine();
        }
    }
}
