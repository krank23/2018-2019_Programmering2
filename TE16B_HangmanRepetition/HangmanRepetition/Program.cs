﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangmanRepetition
{
    class Program
    {
        static void Main(string[] args)
        {
            /*string word = "ninjasköldpadda";
            string[] underscores = GenerateUnderscores(word);

            Console.WriteLine(PrettyPrint(underscores));

            underscores = ReplaceFilter("d", word, underscores);

            Console.WriteLine(PrettyPrint(underscores));*/


            //Console.WriteLine(IsIn("g", "göran"));
            //Console.WriteLine(IsIn("g", "sten"));


            HangmanGame();

            //string[] t = new string[] { "t", "a", "n", "k", "e", "n"};

            //string[] underscores = GenerateUnderscores("ö");

            //underscores[0] = "ö";

            //string g = GetGuess();

            //Console.WriteLine(g);

            //Console.WriteLine(PrettyPrint(underscores));
            //Console.WriteLine(IsComplete(underscores));

            Console.ReadLine();
        }

        static void HangmanGame()
        {
            // FÖRBEREDELSER
            string word = RandomWord();

            string[] visibleWord = GenerateUnderscores(word);

            List<string> erroneousGuesses = new List<string>();

            int maxErroneousGuesses = 7;

            // SPELET
            while (!IsComplete(visibleWord) && erroneousGuesses.Count < maxErroneousGuesses)
            {
                Console.WriteLine(DrawHangedMan(erroneousGuesses.Count));
                Console.WriteLine(PrettyPrint(visibleWord));

                string guess = GetGuess();

                if (IsIn(guess, word))
                {
                    visibleWord = ReplaceFilter(guess, word, visibleWord);

                }
                else
                {
                    erroneousGuesses.Add(guess);
                }
            }

            if (IsComplete(visibleWord))
            {
                DisplayWin();
            }
            else
            {
                DisplayLoss();
            }
        }

        static string[] GenerateUnderscores(string word)
        {
            // Skapa en array med lika många _ som det finns bokstäver/tecken i word

            string[] underscores = new string[word.Length];

            for (int i = 0; i < underscores.Length; i++)
            {
                underscores[i] = "_";
            }
            
            return underscores;
        }

        static string RandomWord()
        {
            // Slumpa ett ord från en lista, bara små bokstäver

            string[] words = new string[] { "apa", "gris", "teacher" };

            Random gen = new Random();

            int i = gen.Next(words.Length);

            return words[i];
        }

        static string PrettyPrint(string[] visibleWord)
        {
            // Returnera en snygg string som genererats utifrån visibleWord.
            // T.ex. om visibleWord är ["a", "p", "a"] så kan man returnera "a p a".

            return string.Join(" ", visibleWord);
        }

        static bool IsComplete(string[] visibleWord)
        {
            // Returnera true ifall alla _ har bytts ut mot tecken, annars false

            int position = Array.IndexOf(visibleWord, "_");

            if (position >= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        static string GetGuess()
        {
            // Returnera en 1 bokstavs gissning. Returnera alltid en liten bokstav (a istället för A t.ex.)
            Console.WriteLine("Plz one lettr:");

            /*string input = Console.ReadLine();
            char c = input[0];

            string inputv2 = c.ToString();
            string inputv3 = inputv2.ToLower();*/

            return Console.ReadLine()[0].ToString().ToLower();
        }

        static bool IsIn(string s, string word)
        {
            // Returnera true om s finns i word, annars false.

            bool result = word.Contains(s);

            return result;
        }

        static string[] ReplaceFilter(string s, string word, string[] visibleWord)
        {
            // Skapa och returnera en kopia av visibleWord där alla positioner där 
            // s finns i word bytts ut mot s.
            // T.ex. om s är "m", word är ["m", "a", "m", "m", "a"] och visibleWord är
            // ["_", "_", "_", "_", "_"] så ska metoden returnera ["m", "_", "m", "m", "_"]

            for (int i = 0; i < word.Length; i++)
            {
                if (word[i] == s[0])
                {
                    visibleWord[i] = s;
                }
            }

            return visibleWord;
        }

        static string DrawHangedMan(int step)
        {
            // Print the hanged man's current status, where step = 0 equals nothing being shown,
            // step = 1 equals the hill being drawn, etc.

            return "Hangman status: " + step;
        }

        static void DisplayWin()
        {
            // Visa någon form av vinst-meddelande
            Console.WriteLine("MAEK HANGMAN LAGOM AGAIN");
        }

        static void DisplayLoss()
        {
            // Visa någon form av förlust-meddelande
            Console.WriteLine("NOOOOOOOOOOOOOOOOooooooOOOO!");
        }

    }
}
