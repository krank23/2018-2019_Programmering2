﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16A_Polyporfism
{
    class Zombie : Human
    {
        public Zombie()
        {
            maxHp = 5;
            hp = maxHp;
        }

        public override void GetHealthPowerup()
        {
            hp = Math.Max(hp - 5, 0);
        }
    }


}
