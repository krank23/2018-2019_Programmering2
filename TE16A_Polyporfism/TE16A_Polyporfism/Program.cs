﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16A_Polyporfism
{
    class Program
    {
        static void Main(string[] args)
        {

            Human h = new Human();

            h.GetHealthPowerup();

            Zombie z = new Zombie();

            z.GetHealthPowerup();


            Human something = new Zombie();

            something.GetHealthPowerup();

            List<Human> people = new List<Human>();

            people.Add(new Human());
            people.Add(new Zombie());

            people[0].GetHealthPowerup();

            
            Console.ReadLine();
        }

        public static void Shout()
        {
            Shout("HEYOOOO!");
        }

        public static void Shout(string what)
        {
            Console.WriteLine(what.ToUpper());
        }

    }
}
