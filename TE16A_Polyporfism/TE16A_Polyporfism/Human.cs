﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TE16A_Polyporfism
{
    class Human
    {
        protected int hp = 10;
        protected int maxHp = 10;

        public virtual void GetHealthPowerup()
        {
            hp = Math.Min(hp + 5, maxHp);
        }
    }

}
